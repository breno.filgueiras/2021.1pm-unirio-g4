package br.uniriotec.pm.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import br.uniriotec.pm.models.Email;
import br.uniriotec.pm.models.NovoEmail;

public class EmailService {
	private static Map<UUID, Email> emails = new HashMap<>();

	EmailService() {
	}

	static {
		UUID idA = UUID.randomUUID();
		UUID idB = UUID.randomUUID();

		emails.put(idA, new Email(idA, new NovoEmail("breno@uniriotec.br", "Devolução de Livro")));
		emails.put(idB, new Email(idB, new NovoEmail("alice@uniriotec.br", "Sobre Canary Release")));
	}

	public static void save(Email email) {
		emails.put(email.getId(), email);
	}

	public static Collection<Email> getAll() {
		return emails.values();
	}

	public static Email findById(UUID emailId) {
		return emails.get(emailId);
	}

	public static void delete(UUID emailId) {
		emails.remove(emailId);
	}
}
