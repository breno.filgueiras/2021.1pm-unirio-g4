package br.uniriotec.pm.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import br.uniriotec.pm.models.CartaoDeCredito;
import br.uniriotec.pm.models.Cobranca;
import br.uniriotec.pm.models.NovaCobranca;
import br.uniriotec.pm.models.NovoCartaoDeCredito;
import br.uniriotec.pm.models.Status;

public class CobrancaService {
	private static Map<UUID, Cobranca> cobrancas = new HashMap<>();
	private static Map<UUID, Cobranca> filaCobrancas = new HashMap<>();
	private static Map<UUID, CartaoDeCredito> cartoes = new HashMap<>();

	private CobrancaService() {
	}

	static {
		UUID idA = UUID.randomUUID();
		UUID idB = UUID.randomUUID();
		UUID idC = UUID.randomUUID();
		UUID idD = UUID.randomUUID();

		NovaCobranca novaCobrancaA = new NovaCobranca((float) 9.0, idA);
		NovaCobranca novaCobrancaB = new NovaCobranca((float) 19.9, idB);

		cobrancas.put(idA, new Cobranca(idA, Status.PAGA, "2021-08-28 10:20:00", "2021-08-29 10:20:00", novaCobrancaA));
		cobrancas.put(idB, new Cobranca(idB, Status.PAGA, "2021-08-27 10:20:00", "2021-08-31 10:20:00", novaCobrancaB));

		NovoCartaoDeCredito cartaoA = new NovoCartaoDeCredito("Eduardo Almeida Campos", "1234 5678 9123 4567", "08/32",
				"564");
		NovoCartaoDeCredito cartaoB = new NovoCartaoDeCredito("Armando Soares de Oliveira", "5678 1234 4567 9123",
				"10/26", "465");

		cartoes.put(idC, new CartaoDeCredito(idC, cartaoA));
		cartoes.put(idD, new CartaoDeCredito(idD, cartaoB));
	}

	/*
	 * Adiciona uma cobrança no hash de cobranças padrão.
	 */
	public static void save(Cobranca cobranca) {
		cobrancas.put(cobranca.getId(), cobranca);
	}

	/*
	 * Adiciona uma cobrança na fila de cobrancas ao inves de adicionar ao hash de cobrancas.
	 */
	public static void saveFila(Cobranca cobranca) {
		filaCobrancas.put(cobranca.getId(), cobranca);
	}

	public static void validaCartao(CartaoDeCredito cartao) {
		cartoes.put(cartao.getId(), cartao);
	}

	public static Collection<Cobranca> getAll() {
		return cobrancas.values();
	}

	public static Collection<Cobranca> getAllFila() {
		return filaCobrancas.values();
	}

	public static Collection<CartaoDeCredito> getAllCartoes() {
		return cartoes.values();
	}

	public static Cobranca findById(UUID cobrancaId) {
		return cobrancas.get(cobrancaId);
	}

	public static void deleteCobranca(UUID id) {
		cobrancas.remove(id);
	}

	public static void deleteCobrancaFila(UUID id) {
		filaCobrancas.remove(id);
	}

	public static void deleteCartao(UUID id) {
		cartoes.remove(id);
	}
}
