package br.uniriotec.pm;

import java.util.UUID;

public class Erro {

	// Attributes
	private UUID id; // UUID
	private String codigo;
	private String mensagem;

	// Constructor
	public Erro(UUID id, String codigo, String mensagem) {
		this.id = id;
		this.codigo = codigo;
		this.mensagem = mensagem;
	}

	// Getters and Setters
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
