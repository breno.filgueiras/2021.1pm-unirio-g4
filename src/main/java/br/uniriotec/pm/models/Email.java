package br.uniriotec.pm.models;

import java.util.UUID;

public class Email {
	// Attributes
	private UUID id;
	private NovoEmail conteudoEmail;

	// Constructor
	public Email(UUID id, NovoEmail email) {
		this.id = id;
		this.conteudoEmail = email;
	}

	// Getters and Setters
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getEmail() {
		return conteudoEmail.getEmail();
	}

	public void setEmail(NovoEmail email) {
		this.conteudoEmail = email;
	}

	public String getMensagem() {
		return conteudoEmail.getMensagem();
	}

	public void setMensagem(String mensagem) {
		this.conteudoEmail.setMensagem(mensagem);
	}

	@Override
	public String toString() {
		return "Email [id=" + id + ", Email=" + conteudoEmail.getEmail() + "]";
	}
	
}
