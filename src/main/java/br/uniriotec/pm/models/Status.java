package br.uniriotec.pm.models;

public enum Status {
	PENDENTE, PAGA, FALHA, CANCELADA, OCUPADA
}
