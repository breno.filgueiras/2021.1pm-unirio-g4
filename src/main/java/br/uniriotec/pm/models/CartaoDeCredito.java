package br.uniriotec.pm.models;

import java.util.UUID;

public class CartaoDeCredito {

	// Attributes
	private UUID id;
	private NovoCartaoDeCredito cartao;

	// Constructors
	public CartaoDeCredito() {
	}

	public CartaoDeCredito(UUID id, NovoCartaoDeCredito cartao) {
		this.id = id;
		this.cartao = cartao;
	}

	// Getters and Setters
	public NovoCartaoDeCredito getCartao() {
		return cartao;
	}

	public void setCvv(NovoCartaoDeCredito cartao) {
		this.cartao = cartao;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CartaoDeCredito [id=" + id + ", cartao=" + cartao.getNumero() + ", " + cartao.getCvv() + "]";
	}

}
