package br.uniriotec.pm.models;

import java.util.UUID;

public class Cobranca {
	// Attributes
	private UUID id; // UUID
	private Status status;
	private String horaSolicitacao; // Date time
	private String horaFinalizacao; // Date time
	private float valor;
	private UUID ciclista;

	// Constructors
	public Cobranca(UUID id, Status status, String horaSolicitacao, NovaCobranca novaCobranca) {
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.valor = novaCobranca.getValor();
		this.ciclista = novaCobranca.getCiclista();
	}

	public Cobranca(UUID id, Status status, String horaSolicitacao, String horaFinalizacao, NovaCobranca novaCobranca) {
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = novaCobranca.getValor();
		this.ciclista = novaCobranca.getCiclista();
	}

	// Getters and Setters
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getHoraSolicitacao() {
		return horaSolicitacao;
	}

	public void setHoraSolicitacao(String horaSolicitacao) {
		this.horaSolicitacao = horaSolicitacao;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public UUID getCiclista() {
		return ciclista;
	}

	public void setCiclista(UUID ciclista) {
		this.ciclista = ciclista;
	}

	public String getHoraFinalizacao() {
		return horaFinalizacao;
	}

	public void setHoraFinalizacao(String horaFinalizacao) {
		this.horaFinalizacao = horaFinalizacao;
	}

	@Override
	public String toString() {
		return "Cobranca [id=" + id + ", status=" + status + ", horaSolicitacao=" + horaSolicitacao
				+ ", horaFinalizacao=" + horaFinalizacao + ", valor=" + valor + ", ciclista=" + ciclista + "]";
	}

}
