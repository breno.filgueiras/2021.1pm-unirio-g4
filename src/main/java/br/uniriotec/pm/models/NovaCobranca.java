package br.uniriotec.pm.models;

import java.util.UUID;

public class NovaCobranca {

	// Attributes
	private float valor;
	private UUID ciclista;

	// Constructors
	public NovaCobranca() {
	}

	public NovaCobranca(float valor, UUID ciclista) {
		this.valor = valor;
		this.ciclista = ciclista;
	}
	
	// Getters and Setters
	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public UUID getCiclista() {
		return ciclista;
	}

	public void setCiclista(UUID ciclista) {
		this.ciclista = ciclista;
	}

}
