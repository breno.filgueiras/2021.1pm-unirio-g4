package br.uniriotec.pm.models;

public class NovoEmail {

	// Attributes
	private String email;
	private String mensagem;

	// Constructors
	public NovoEmail() {
	}

	public NovoEmail(String email, String mensagem) {
		this.email = email;
		this.mensagem = mensagem;
	}

	// Getters and Setters
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
