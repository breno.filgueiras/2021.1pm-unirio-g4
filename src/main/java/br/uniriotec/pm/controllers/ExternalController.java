package br.uniriotec.pm.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import br.uniriotec.pm.Erro;
import br.uniriotec.pm.models.CartaoDeCredito;
import br.uniriotec.pm.models.Cobranca;
import br.uniriotec.pm.models.Email;
import br.uniriotec.pm.models.NovaCobranca;
import br.uniriotec.pm.models.NovoCartaoDeCredito;
import br.uniriotec.pm.models.NovoEmail;
import br.uniriotec.pm.models.Status;
import br.uniriotec.pm.services.CobrancaService;
import br.uniriotec.pm.services.EmailService;
// Javalin imports
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiParam;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;

public class ExternalController {

	private static final String EMAIL_NOT_VALID = "Email não é válido";
	private static final String COBRANCA_NOT_FOUND = "Cobrança não encontrada.";
	private static final String COBRANCA_NOT_VALID = "Cobrança não é válida.";
	private static final String CARTAO_NOT_VALID = "Cartão não é válido.";

	private ExternalController() {
	}

	@OpenApi(summary = "Notificar via email", operationId = "sendEmail", path = "/enviarEmail", method = HttpMethod.POST, tags = {
			"Externo" }, requestBody = @OpenApiRequestBody(content = {
					@OpenApiContent(from = NovoEmail.class) }), responses = {
							@OpenApiResponse(status = "200", content = { @OpenApiContent(from = Email.class) }),
							@OpenApiResponse(status = "400", content = { @OpenApiContent(from = Erro.class) }) })
	public static Email enviarEmail(Context ctx) {
		NovoEmail novoEmail = ctx.bodyAsClass(NovoEmail.class);
		
		if (validaEmail(novoEmail.getEmail())) {
			Email email = new Email(UUID.randomUUID(), novoEmail);
			EmailService.save(email);
			// success
			ctx.json(email);
			return email;
		} else {
			throw new NotFoundResponse(EMAIL_NOT_VALID);
		}
	}

	@OpenApi(summary = "Realizar Cobrança", operationId = "cobranca", path = "/cobranca", method = HttpMethod.POST, tags = {
			"Externo" }, requestBody = @OpenApiRequestBody(content = {
					@OpenApiContent(from = NovaCobranca.class) }), responses = {
							@OpenApiResponse(status = "200", content = { @OpenApiContent(from = Cobranca.class) }),
							@OpenApiResponse(status = "422", content = { @OpenApiContent(from = Erro.class) }) })
	public static Cobranca cobranca(Context ctx) {
		NovaCobranca novaCobranca = ctx.bodyAsClass(NovaCobranca.class);

		if (cobrancaValida(novaCobranca)) {
			Date now = new Date();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
			Cobranca cobranca = new Cobranca(UUID.randomUUID(), Status.PENDENTE, df.format(now), novaCobranca);
			CobrancaService.save(cobranca);
			// success
			ctx.json(cobranca);
			return cobranca;
		} else {
			throw new NotFoundResponse(COBRANCA_NOT_VALID);
		}
	}

	@OpenApi(summary = "Inclui cobranca na fila de cobrança. Cobranças na fila serão cobradas de tempos em tempos.", operationId = "filaCobranca", path = "/filaCobranca", method = HttpMethod.POST, tags = {
			"Externo" }, requestBody = @OpenApiRequestBody(content = {
					@OpenApiContent(from = NovaCobranca.class) }), responses = {
							@OpenApiResponse(status = "200", content = { @OpenApiContent(from = Cobranca.class) }),
							@OpenApiResponse(status = "422", content = { @OpenApiContent(from = Erro.class) }) })
	public static Cobranca filaCobranca(Context ctx) {
		NovaCobranca novaCobranca = ctx.bodyAsClass(NovaCobranca.class);

		if (cobrancaValida(novaCobranca)) {
			Date now = new Date();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

			Cobranca cobranca = new Cobranca(UUID.randomUUID(), Status.PENDENTE, df.format(now), novaCobranca);

			CobrancaService.saveFila(cobranca);

			// success
			ctx.json(cobranca);
			return cobranca;
		} else {
			throw new NotFoundResponse(COBRANCA_NOT_VALID);
		}
	}

	@OpenApi(summary = "Obter cobrança.", operationId = "getCobranca", path = "/cobranca/:idCobranca", method = HttpMethod.GET, pathParams = {
			@OpenApiParam(name = "idCobranca", type = String.class, description = "Id da Cobranca") }, tags = {
					"Externo" }, responses = {
							@OpenApiResponse(status = "200", content = { @OpenApiContent(from = Cobranca.class) }),
							@OpenApiResponse(status = "404", content = { @OpenApiContent(from = Erro.class) }) })
	public static Cobranca getCobranca(Context ctx) {
		Cobranca cobranca = CobrancaService.findById(validaCobrancaId(ctx));

		if (cobranca == null) {
			throw new NotFoundResponse(COBRANCA_NOT_FOUND);
		} else {
			ctx.json(cobranca);
			return cobranca;
		}
	}

	@OpenApi(summary = "Valida um cartão de crédito", operationId = "validaCartao", path = "/validaCartaoDeCredito", method = HttpMethod.POST, tags = {
			"Externo" }, requestBody = @OpenApiRequestBody(content = {
					@OpenApiContent(from = NovoCartaoDeCredito.class) }), responses = {
							@OpenApiResponse(status = "200"),
							@OpenApiResponse(status = "422", content = { @OpenApiContent(from = Erro.class) }),
							@OpenApiResponse(status = "404", content = { @OpenApiContent(from = Erro.class) }) })
	public static CartaoDeCredito validaCartaoDeCredito(Context ctx) {

		NovoCartaoDeCredito novoCartao = ctx.bodyAsClass(NovoCartaoDeCredito.class);

		if (cartaoValido(novoCartao)) {
			CartaoDeCredito cartao = new CartaoDeCredito(UUID.randomUUID(), novoCartao);
			CobrancaService.validaCartao(cartao);
			// success
			ctx.json(cartao);
			return cartao;
		} else {
			throw new NotFoundResponse(CARTAO_NOT_VALID);
		}
	}

// 	 Aux methods

	private static boolean validaEmail(String email) {
		if (email.contains("@")) {
			return true;
		} else {
			return false;
		}
	}

	private static boolean cobrancaValida(NovaCobranca cobranca) {

		String idCiclista = cobranca.getCiclista().toString();

		if ((cobranca.getValor() < 0) || idCiclista.equals("") || idCiclista.equals(" ")) {
			return false;
		} else {
			return true;
		}

	}

	private static boolean cartaoValido(NovoCartaoDeCredito cartao) {

		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("mm/yy");
		int mes = Integer.parseInt(formatter.format(date).substring(0, 2));
		int ano = Integer.parseInt(formatter.format(date).substring(3, 4));

		// formatando os dados para checarmos a validade do cartao
		int mesCartao = Integer.parseInt(cartao.getValidade().substring(0, 2));
		int anoCartao = Integer.parseInt(cartao.getValidade().substring(3, 4));
		String titular = cartao.getNomeTitular();
		String cvv = cartao.getCvv().replace(" ", "").replace("-", "");
		String numero = cartao.getNumero().replace(" ", "");

		/*
		 * Casos de invalidação: 1) Mes e Ano atuais superiores aos da validade, 2) Mes atual inferior ao da validade e
		 * Ano atual superior ao da validade, 3) Mes atual superior ao da validade e Ano atual igual ao da validade
		 * 
		 * O total de digitos em cartões de crédito está sendo considerado 16
		 * 
		 */
		if ((mes > mesCartao && ano > anoCartao) || (mes < mesCartao && ano > anoCartao)
				|| (mes > mesCartao && ano == anoCartao)) {
			// cartao fora da validade
			return false;
		} else if (titular.equals("") || titular.equals(" ")) {
			// Nome em branco
			return false;
		} else if ((numero.length() != 16) || (cvv.length() < 3) || (cvv.length() > 4)) {
			// numero do cartao ou cvv fora dos padrões
			return false;
		} else {
			return true;
		}

	}

	private static UUID validaCobrancaId(Context ctx) {
		return UUID.fromString(ctx.pathParam("idCobranca", String.class)
				.check(idCobranca -> (!idCobranca.equals("") || !idCobranca.equals(" ") || (idCobranca != null)))
				.get());

	}
}
