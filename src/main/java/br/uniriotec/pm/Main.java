package br.uniriotec.pm;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

// Controller Imports
import br.uniriotec.pm.controllers.ExternalController;
// Javalin.io Imports
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

public class Main {

	public static void main(String[] args) {

		// new comment
		Javalin.create(config -> {
			config.registerPlugin(getConfiguredOpenApiPlugin());
			config.defaultContentType = "application/json";
		}).routes(() -> {

			path("enviarEmail", () -> post(ExternalController::enviarEmail));

			path("cobranca", () -> {
				post(ExternalController::cobranca);
				path(":idCobranca", () -> get(ExternalController::getCobranca));
			});

			path("filaCobranca", () -> post(ExternalController::filaCobranca));

			path("validaCartaoDeCredito", () -> post(ExternalController::validaCartaoDeCredito));

		}).get("/", ctx -> ctx.redirect("swagger-ui")).start(getHerokuAssignedPort());

	}

	public static int getHerokuAssignedPort() {
		ProcessBuilder processBuilder = new ProcessBuilder();
		if (processBuilder.environment().get("PORT") != null) {
			return Integer.parseInt(processBuilder.environment().get("PORT"));
		}
		return 7000;
	}

	public static OpenApiPlugin getConfiguredOpenApiPlugin() {

		Info info = new Info().version("1.0").title("PM-Brenofil-G4 :: API")
				.description("API para testar o projeto em javalin integrando com o Sonar e o Heroku.");

		OpenApiOptions options = new OpenApiOptions(info).activateAnnotationScanningFor("br.uniriotec.pm")
				.path("/swagger-docs") // endpoint for OpenAPI json
				.swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
				.reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
				.defaultDocumentation(doc -> {
					doc.json("500", Erro.class);
					doc.json("503", Erro.class);
				});

		return new OpenApiPlugin(options);
	}
}