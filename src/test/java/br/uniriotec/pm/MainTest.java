package br.uniriotec.pm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiPlugin;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainTest {

	private Javalin app = Javalin.create();

	@Test
	public void A_CHECK_APP_START_TEST() {
		app.start(5555);
		assertEquals(5555, app.port());
	}

	@Test
	public void B_GET_ASSIGNED_PORT_TEST() {
		assertEquals(7000, Main.getHerokuAssignedPort());
	}

	@Test
	public void C_GET_CONFIGURED_API_PLUGIN_TEST() {
		assertNotNull(Main.getConfiguredOpenApiPlugin());
	}
}