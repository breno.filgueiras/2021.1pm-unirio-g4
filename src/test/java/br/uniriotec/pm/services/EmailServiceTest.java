package br.uniriotec.pm.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.UUID;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.uniriotec.pm.models.Email;
import br.uniriotec.pm.models.NovoEmail;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class EmailServiceTest {

	private static Email emailTeste = new Email(UUID.randomUUID(),
			new NovoEmail("teste@teste.com.br", "mensagem de teste"));

	@Test
	public void A_SAVE_TEST() {
		int totalEmails = EmailService.getAll().size();
		EmailService.save(emailTeste);
		assertEquals("Adicionando mais 1 email...", (totalEmails + 1), EmailService.getAll().size());

		System.out.println("SAVE_TEST......");
		Collection<Email> emails = EmailService.getAll();
		for (Email email : emails) {
			System.out.println("SAVE_TEST :: email: " + email);
		}
		System.out.println("FIM :: SAVE_TEST......");
	}

	@Test
	public void B_GET_ALL_TEST() {
		System.out.println("GET_ALL_TEST......");
		Collection<Email> emails = EmailService.getAll();
		for (Email email : emails) {
			System.out.println("GET_ALL_TEST :: email: " + email);
			assertNotNull(email.getEmail());
		}
		System.out.println("FIM :: GET_ALL_TEST......");
	}

	@Test
	public void C_FIND_BY_ID_TEST() {

		Email emailBuscado = EmailService.findById(emailTeste.getId());

		System.out.println("FIND_BY_ID_TEST......");
		Collection<Email> emails = EmailService.getAll();
		for (Email email : emails) {
			System.out.println("FIND_BY_ID_TEST :: email: " + email);
		}
		System.out.println("FIM :: FIND_BY_ID_TEST......");

		assertEquals("Buscando email...", emailTeste.getId(), emailBuscado.getId());
	}

	@Test
	public void D_DELETE_TEST() {

		System.out.println("BEFORE DELETE_TEST......");
		Collection<Email> emails = EmailService.getAll();
		for (Email email : emails) {
			System.out.println("BEFORE DELETE_TEST :: email: " + email);
		}
		System.out.println("FIM :: BEFORE DELETE_TEST......");

		int totalEmails = EmailService.getAll().size();

		EmailService.delete(emailTeste.getId());

		Collection<Email> emailsAfter = EmailService.getAll();
		System.out.println("AFTER DELETE_TEST......");
		for (Email email : emailsAfter) {
			System.out.println("AFTER DELETE_TEST :: email: " + email);
		}
		System.out.println("FIM :: AFTER DELETE_TEST......");

		assertEquals("Removendo email...", (totalEmails - 1), EmailService.getAll().size());

	}
	
	@Test
	public void E_CONSTRUCTOR_TEST() {
		EmailService service = new EmailService();
		assertNotNull(service);
	}
}
