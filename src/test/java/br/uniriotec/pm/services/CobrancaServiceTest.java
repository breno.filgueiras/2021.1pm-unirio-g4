package br.uniriotec.pm.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.UUID;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.uniriotec.pm.models.CartaoDeCredito;
import br.uniriotec.pm.models.Cobranca;
import br.uniriotec.pm.models.NovaCobranca;
import br.uniriotec.pm.models.NovoCartaoDeCredito;
import br.uniriotec.pm.models.Status;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CobrancaServiceTest {

	private static Cobranca cobrancaTeste = new Cobranca(UUID.randomUUID(), Status.PAGA, "05/09/2021 15:35:23",
			"05/09/2021 15:36:28", new NovaCobranca((float) 10.5, UUID.randomUUID()));

	private static CartaoDeCredito cartaoTeste = new CartaoDeCredito(UUID.randomUUID(),
			new NovoCartaoDeCredito("Teste Teste", "1234 1234 1234 1234", "11/25", "123"));

	@Test
	public void A_SAVE_TEST() {
		int totalCobrancas = CobrancaService.getAll().size();

		CobrancaService.save(cobrancaTeste);

		assertEquals("Adicionando mais 1 cobranca...", (totalCobrancas + 1), CobrancaService.getAll().size());

		System.out.println("SAVE_TEST......");
		Collection<Cobranca> cobrancas = CobrancaService.getAll();
		for (Cobranca cobranca : cobrancas) {
			System.out.println("SAVE_TEST :: cobranca: " + cobranca);
		}
		System.out.println("FIM :: SAVE_TEST......");
	}

	@Test
	public void B_ADD_TEST() {
		int totalCobrancas = CobrancaService.getAllFila().size();

		CobrancaService.saveFila(cobrancaTeste);

		assertEquals("Adicionando mais 1 cobranca...", (totalCobrancas + 1), CobrancaService.getAllFila().size());

		System.out.println("SAVE_TEST......");
		Collection<Cobranca> cobrancas = CobrancaService.getAllFila();
		for (Cobranca cobranca : cobrancas) {
			System.out.println("SAVE_TEST :: cobranca: " + cobranca);
		}
		System.out.println("FIM :: SAVE_TEST......");
	}

	@Test
	public void C_VALIDA_CARTAO_TEST() {
		int totalCartoes = CobrancaService.getAllCartoes().size();
		CobrancaService.validaCartao(cartaoTeste);
		assertEquals("Adicionando mais 1 email...", (totalCartoes + 1), CobrancaService.getAllCartoes().size());

		System.out.println("SAVE_TEST......");
		Collection<CartaoDeCredito> cartoes = CobrancaService.getAllCartoes();
		for (CartaoDeCredito cartao : cartoes) {
			System.out.println("SAVE_TEST :: cartao: " + cartao);
		}
		System.out.println("FIM :: SAVE_TEST......");
	}

	@Test
	public void D_GET_ALL_TEST() {
		System.out.println("GET_ALL_COBRANCAS_TEST......");
		Collection<Cobranca> cobrancas = CobrancaService.getAll();
		for (Cobranca cobranca : cobrancas) {
			System.out.println("GET_ALL_COBRANCAS_TEST :: cobranca: " + cobranca);
			// verifica se tem ao menos um valor e um ciclista
			assertNotNull(cobranca.getCiclista());
		}
		System.out.println("FIM :: GET_ALL_COBRANCAS_TEST......");
	}

	@Test
	public void E_GET_ALL_FILA_TEST() {
		System.out.println("GET_ALL_FILA_TEST......");
		Collection<Cobranca> cobrancas = CobrancaService.getAllFila();
		for (Cobranca cobranca : cobrancas) {
			System.out.println("GET_ALL_FILA_TEST :: cobranca: " + cobranca);
			// verifica se tem ao menos um valor e um ciclista
			assertNotNull(cobranca.getCiclista());
		}
		System.out.println("FIM :: GET_ALL_FILA_TEST......");
	}

	@Test
	public void F_GET_ALL_CARTOES_TEST() {
		System.out.println("GET_ALL_CARTOES_TEST......");
		Collection<CartaoDeCredito> cartoes = CobrancaService.getAllCartoes();
		for (CartaoDeCredito cartao : cartoes) {
			System.out.println("GET_ALL_CARTOES_TEST :: cartao: " + cartao);
			assertNotNull(cartao.getCartao().getNumero());
		}
		System.out.println("FIM :: GET_ALL_CARTOES_TEST......");
	}

	@Test
	public void G_FIND_BY_ID_TEST() {

		Cobranca cobrancaBuscada = CobrancaService.findById(cobrancaTeste.getId());

		System.out.println("FIND_BY_ID_TEST......");
		Collection<Cobranca> cobrancas = CobrancaService.getAll();
		for (Cobranca cobranca : cobrancas) {
			System.out.println("FIND_BY_ID_TEST :: email: " + cobranca);
		}
		System.out.println("FIM :: FIND_BY_ID_TEST......");

		assertEquals("Buscando email...", cobrancaTeste.getId(), cobrancaBuscada.getId());
	}

	@Test
	public void H_DELETE_COBRANCA_TEST() {

		System.out.println("BEFORE DELETE_COBRANCA_TEST......");
		Collection<Cobranca> cobrancas = CobrancaService.getAll();
		for (Cobranca cobranca : cobrancas) {
			System.out.println("BEFORE DELETE_COBRANCA_TEST :: cobranca: " + cobranca);
		}
		System.out.println("FIM :: BEFORE DELETE_COBRANCA_TEST......");

		int totalCobrancas = CobrancaService.getAll().size();

		CobrancaService.deleteCobranca(cobrancaTeste.getId());

		Collection<Cobranca> cobrancasAfter = CobrancaService.getAll();
		System.out.println("AFTER DELETE_COBRANCA_TEST......");
		for (Cobranca cobranca : cobrancasAfter) {
			System.out.println("AFTER DELETE_COBRANCA_TEST :: cobranca: " + cobranca);
		}
		System.out.println("FIM :: AFTER DELETE_COBRANCA_TEST......");

		assertEquals("Removendo cobranca...", (totalCobrancas - 1), CobrancaService.getAll().size());

	}

	@Test
	public void I_DELETE_CARTAO_TEST() {

		System.out.println("BEFORE DELETE_CARTAO_TEST......");
		Collection<CartaoDeCredito> cartoes = CobrancaService.getAllCartoes();
		for (CartaoDeCredito cartao : cartoes) {
			System.out.println("BEFORE DELETE_CARTAO_TEST :: cobranca: " + cartao);
		}
		System.out.println("FIM :: BEFORE DELETE_CARTAO_TEST......");

		int totalCartoes = CobrancaService.getAllCartoes().size();

		CobrancaService.deleteCartao(cartaoTeste.getId());

		Collection<CartaoDeCredito> cartoesAfter = CobrancaService.getAllCartoes();
		System.out.println("AFTER DELETE_CARTAO_TEST......");
		for (CartaoDeCredito cartao : cartoesAfter) {
			System.out.println("AFTER DELETE_CARTAO_TEST :: cartao: " + cartao);
		}
		System.out.println("FIM :: AFTER DELETE_CARTAO_TEST......");

		assertEquals("Removendo cartao...", (totalCartoes - 1), CobrancaService.getAllCartoes().size());

	}

	@Test
	public void J_DELETE_COBRANCA_FILA_TEST() {

		System.out.println("BEFORE DELETE_COBRANCA_FILA_TEST......");
		Collection<Cobranca> fila = CobrancaService.getAllFila();
		for (Cobranca cobranca : fila) {
			System.out.println("BEFORE DELETE_COBRANCA_FILA_TEST :: cobranca: " + cobranca);
		}
		System.out.println("FIM :: BEFORE DELETE_COBRANCA_FILA_TEST......");

		int totalCobrancas = CobrancaService.getAllFila().size();

		CobrancaService.deleteCobrancaFila(cobrancaTeste.getId());

		Collection<Cobranca> cobrancaAfter = CobrancaService.getAllFila();
		System.out.println("AFTER DELETE_COBRANCA_FILA_TEST......");
		for (Cobranca cobranca : cobrancaAfter) {
			System.out.println("AFTER DELETE_COBRANCA_FILA_TEST :: cartao: " + cobranca);
		}
		System.out.println("FIM :: AFTER DELETE_COBRANCA_FILA_TEST......");

		assertEquals("Removendo cartao...", (totalCobrancas - 1), CobrancaService.getAllFila().size());

	}

}
