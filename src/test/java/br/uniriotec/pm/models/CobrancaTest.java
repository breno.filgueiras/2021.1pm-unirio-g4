package br.uniriotec.pm.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CobrancaTest {

	private static NovaCobranca novaCobranca = new NovaCobranca((float) 10.20, UUID.randomUUID());
	private static Cobranca cobranca = new Cobranca(UUID.randomUUID(), Status.PAGA, "2021-08-28 10:20:00",
			"2021-08-29 10:20:00", novaCobranca);

	@Test
	public void A_ERRO_PRIMEIRO_CONSTRUCTOR_TEST() {
		Cobranca teste = new Cobranca(UUID.randomUUID(), Status.PAGA, "2021-08-28 10:20:00", "2021-08-29 10:20:00",
				novaCobranca);
		assertNotNull(teste);
	}

	@Test
	public void B_ERRO_SEGUNDO_CONSTRUCTOR_TEST() {
		Cobranca teste = new Cobranca(UUID.randomUUID(), Status.PAGA, "2021-08-28 10:20:00", novaCobranca);
		assertNotNull(teste);
	}

	@Test
	public void C_GET_ID_TEST() {
		assertNotEquals(UUID.randomUUID(), cobranca.getId());
	}

	@Test
	public void D_GET_STATUS_TEST() {
		assertEquals(Status.PAGA, cobranca.getStatus());
	}

	@Test
	public void E_GET_HORA_SOLICITACAO_TEST() {
		assertNotNull(cobranca.getHoraSolicitacao());
	}

	@Test
	public void F_GET_HORA_FINALIZACAO() {
		assertNotNull(cobranca.getHoraFinalizacao());
	}

	@Test
	public void G_GET_VALOR() {
		assertTrue(cobranca.getValor() >= 0.0);
	}

	@Test
	public void H_GET_CICLISTA() {
		assertNotNull(cobranca.getCiclista());
	}

	@Test
	public void I_SET_ID_TEST() {
		UUID id = UUID.randomUUID();
		cobranca.setId(id);
		assertEquals(id, cobranca.getId());
	}

	@Test
	public void J_SET_STATUS_TEST() {
		Status status = Status.CANCELADA;
		cobranca.setStatus(status);
		assertEquals(status, cobranca.getStatus());
	}

	@Test
	public void K_SET_HORA_SOLICITACAO_TEST() {
		String solicitacao = "foo";
		cobranca.setHoraSolicitacao(solicitacao);
		assertEquals(solicitacao, cobranca.getHoraSolicitacao());
	}

	@Test
	public void L_SET_HORA_FINALIZACAO_TEST() {
		String finalizacao = "foo";
		cobranca.setHoraFinalizacao(finalizacao);
		assertEquals(finalizacao, cobranca.getHoraFinalizacao());
	}

	@Test
	public void M_SET_VALOR_TEST() {
		float valor = (float) 100.30;
		float delta = valor - cobranca.getValor();
		cobranca.setValor(valor);
		assertEquals(valor, cobranca.getValor(), delta);
	}

	@Test
	public void N_SET_CICLISTA() {
		UUID ciclista = UUID.randomUUID();
		cobranca.setCiclista(ciclista);
		assertEquals(ciclista, cobranca.getCiclista());
	}

}
