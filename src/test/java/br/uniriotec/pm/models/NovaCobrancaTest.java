package br.uniriotec.pm.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NovaCobrancaTest {

	private static NovaCobranca novaCobranca = new NovaCobranca((float) 10.55, UUID.randomUUID());

	@Test
	public void A_CONSTRUCTOR_TEST() {
		NovaCobranca aux = new NovaCobranca();
		assertNotNull(aux);
	}

	@Test
	public void B_CONSTRUCTOR_TEST() {
		NovaCobranca aux = new NovaCobranca((float) 2.50, UUID.randomUUID());
		assertNotNull(aux);
	}

	@Test
	public void C_GET_VALOR_TEST() {
		assertTrue(novaCobranca.getValor() >= 0);
	}

	@Test
	public void D_GET_CICLISTA_TEST() {
		assertNotNull(novaCobranca.getCiclista());
	}

	@Test
	public void E_SET_VALOR_TEST() {
		float valor = (float) 2.34;
		novaCobranca.setValor(valor);
		float delta = valor - novaCobranca.getValor();
		assertEquals(valor, novaCobranca.getValor(), delta);
	}

	@Test
	public void F_SET_CICLISTA_TEST() {
		UUID id = UUID.randomUUID();
		novaCobranca.setCiclista(id);
		assertEquals(id, novaCobranca.getCiclista());
	}

}
