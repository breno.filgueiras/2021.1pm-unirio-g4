package br.uniriotec.pm.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NovoCartaoDeCreditoTest {
	private NovoCartaoDeCredito novoCartao = new NovoCartaoDeCredito("Teste", "1234 1234 1234 1234", "10/22", "123");

	@Test
	public void A_CONSTRUCTOR_TEST() {
		NovoCartaoDeCredito novoCartao = new NovoCartaoDeCredito();
		assertNotNull(novoCartao);
	}

	@Test
	public void B_CONSTRUCTOR_TEST() {
		NovoCartaoDeCredito novoCartao = new NovoCartaoDeCredito("Teste", "5678 5678 5678 5678", "09/26", "567");
		assertNotNull(novoCartao);
	}

	@Test
	public void C_GET_TITULAR_TEST() {
		assertNotNull(novoCartao.getNomeTitular());
	}

	@Test
	public void D_GET_NUMERO_TEST() {
		assertNotNull(novoCartao.getNumero());
	}

	@Test
	public void E_GET_VALIDADE_TEST() {
		assertNotNull(novoCartao.getValidade());
	}

	@Test
	public void F_GET_CVV_TEST() {
		assertNotNull(novoCartao.getCvv());
	}

	@Test
	public void setNomeTitular() {
		String titular = "foo";
		novoCartao.setNomeTitular(titular);
		assertEquals(titular, novoCartao.getNomeTitular());
	}

	@Test
	public void setNumero() {
		String numero = "1234 5678 9012 3456";
		novoCartao.setNumero(numero);
		assertEquals(numero, novoCartao.getNumero());
	}

	@Test
	public void setValidade() {
		String validade = "02/84";
		novoCartao.setValidade(validade);
		assertEquals(validade, novoCartao.getValidade());
	}

	@Test
	public void setCvv() {
		String cvv = "423";
		novoCartao.setCvv(cvv);
		assertEquals(cvv, novoCartao.getCvv());
	}
}
