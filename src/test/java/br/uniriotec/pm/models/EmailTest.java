package br.uniriotec.pm.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmailTest {

	private NovoEmail novoEmail = new NovoEmail("teste@foo.bar.br", "teste");
	private Email email = new Email(UUID.randomUUID(), novoEmail);

	@Test
	public void A_CONSTRUCTOR_TEST() {
		Email email = new Email(UUID.randomUUID(), novoEmail);
		assertNotNull(email);
	}

	@Test
	public void B_GET_ID_TEST() {
		assertNotNull(email.getId());
	}

	@Test
	public void C_GET_EMAIL_TEST() {
		assertNotNull(email.getEmail());
	}

	@Test
	public void D_GET_MENSAGEM_TEST() {
		assertNotNull(email.getMensagem());
	}

	@Test
	public void E_SET_ID_TEST() {
		UUID id = UUID.randomUUID();
		email.setId(id);
		assertEquals(id, email.getId());
	}

	@Test
	public void F_SET_EMAIL_TEST() {
		NovoEmail novoEmail = new NovoEmail("foo", "bar");
		email.setEmail(novoEmail);
		assertEquals(novoEmail.getEmail(), email.getEmail());
	}

	@Test
	public void G_SET_MENSAGEM_TEST() {
		String mensagem = "foo";
		email.setMensagem(mensagem);
		assertEquals(mensagem, email.getMensagem());
	}
}
