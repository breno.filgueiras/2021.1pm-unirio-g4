package br.uniriotec.pm.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.UUID;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CartaoDeCreditoTest {

	private NovoCartaoDeCredito novoCartao = new NovoCartaoDeCredito("Teste", "1234 1234 1234 1234", "10/22", "123");
	private CartaoDeCredito cartao = new CartaoDeCredito(UUID.randomUUID(), novoCartao);
	
	@Test
	public void A_CONSTRUCTOR_TEST() {
		CartaoDeCredito auxCartao = new CartaoDeCredito();
		assertNotNull(auxCartao);
	}
	
	@Test
	public void B_CONSTRUCTOR_TEST() {
		CartaoDeCredito auxCartao = new CartaoDeCredito(UUID.randomUUID(), novoCartao);
		assertNotNull(auxCartao);
	}

	@Test
	public void C_GET_CARTAO_TEST() {
		assertNotNull(cartao.getCartao());
	}

	@Test
	public void D_GET_ID_TEST() {
		assertNotNull(cartao.getId());
	}

	@Test
	public void E_SET_CVV_TEST() {
		NovoCartaoDeCredito auxNovo = new NovoCartaoDeCredito("Aux", "5678 5678 5678 5678", "04/21", "456");
		cartao.setCvv(auxNovo);
		assertNotEquals("123", cartao.getCartao().getCvv());
	}

	@Test
	public void F_SET_ID_TEST() {
		UUID id = UUID.randomUUID();
		cartao.setId(id);
		assertEquals(id, cartao.getId());
	}
}
