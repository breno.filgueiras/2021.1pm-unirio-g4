package br.uniriotec.pm.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NovoEmailTest {

	private NovoEmail novoEmail = new NovoEmail("teste@foo.bar.br", "teste");

	@Test
	public void A_CONSTRUCTOR_TEST() {
		NovoEmail aux = new NovoEmail();
		assertNotNull(aux);
	}

	@Test
	public void B_CONSTRUCTOR_TEST() {
		NovoEmail aux = new NovoEmail("foo@bar.com.br", "foo");
		assertNotNull(aux);
	}

	@Test
	public void C_GET_EMAIL_TEST() {
		assertNotNull(novoEmail.getEmail());
	}

	@Test
	public void D_GET_MENSAGEM_TEST() {
		assertNotNull(novoEmail.getMensagem());
	}

	@Test
	public void E_SET_EMAIL_TEST() {
		String email = "bar@teste.foo.br";
		novoEmail.setEmail(email);
		assertEquals(email, novoEmail.getEmail());
	}

	@Test
	public void F_SET_MENSAGEM_TEST() {
		String msg = "foo";
		novoEmail.setMensagem(msg);
		assertEquals(msg, novoEmail.getMensagem());
	}
}
