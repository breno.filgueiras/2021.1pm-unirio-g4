package br.uniriotec.pm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.Mockito;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ErroTest {

	@Mock
	private Erro mockedErro;

	private static Erro auxErro = new Erro(UUID.randomUUID(), "foo", "bar");

	@Test
	public void A_ERRO_CONSTRUCTOR_TEST() {
		Erro teste = new Erro(UUID.randomUUID(), "foo", "bar");
		assertNotNull(teste);
	}

	@Test
	public void B_ERRO_ID_TEST_not_equals() {
		assertNotEquals(UUID.randomUUID(), auxErro.getId());
	}

	@Test
	public void C_ERRO_CODIGO_TEST() {
		assertEquals("foo", auxErro.getCodigo());
	}

	@Test
	public void D_ERRO_MENSAGEM_TEST() {
		assertEquals("bar", auxErro.getMensagem());
	}

	@Test
	public void E_ERRO_SET_ID_TEST() {
		UUID id = UUID.randomUUID();
		auxErro.setId(id);
		assertEquals(id, auxErro.getId());
	}

	@Test
	public void F_ERRO_SET_CODIGO_TEST() {
		String codigo = "208";
		auxErro.setCodigo(codigo);
		assertEquals("208", auxErro.getCodigo());
	}

	@Test
	public void G_ERRO_SET_MENSAGEM_TEST() {
		String msg = "teste";
		auxErro.setMensagem(msg);
		assertEquals("teste", auxErro.getMensagem());
	}
}
